﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceTrackSim
{
    class Bet
    {
        //the amound of the bet
        public int _amount;

        //the race houund number
        public int _raceHound;

        //the bettor object
        public Bettor _bettor;
        public Bettor bettor
        {
            get { return _bettor; }
            set { _bettor = value; }
        }


        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }



        public int RaceHound
        {
            get { return _raceHound; }
            set { _raceHound = value; }
        }


        /// <summary>
        /// Payouut the winner of the race
        /// </summary>
        /// <param name="winner"></param>
        /// <returns></returns>
        public int PayOut(int winner)
        {

            if (_raceHound == winner)
            {
                //Return whatverer their bet amound is times 2
                return _amount;
            }

            //This is retured whatever the amount they bet is, minus that from their cash
            return -_amount;
        }
    }
}
