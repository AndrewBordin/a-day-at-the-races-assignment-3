﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;



// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace RaceTrackSim
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public  partial class MainPage : Page
    {
        //The randomizer for the movement for the hounds
        private Random _randomizer;

        //The list of greyhound objects
        private List<Greyhound> _raceHoundList;

        //The bet object
        private Bet bet;

        //The list of bettor objjects
        private List<Bettor> _crtSelBettor;

        //The timer that control hound movement
        private DispatcherTimer _tmRaceTimer;

        //The minimum placed bet
        int minBet = 5;

        //boolean to check if a bet has been placed
        bool BetPlaced = false;


        public MainPage()
        {
            this.InitializeComponent();

            _randomizer = new Random();

            _raceHoundList = new List<Greyhound>();

            _crtSelBettor = new List<Bettor>();

            bet = new Bet();

            _tmRaceTimer = new DispatcherTimer();
            _tmRaceTimer.Interval = TimeSpan.FromMilliseconds(10);

            //setup the event handler that is going to be called by the timer
            _tmRaceTimer.Tick += OnRaceTimerTick;

            //Adding choices to the combobox
            _comboBoxHound.Items.Add("1");
            _comboBoxHound.Items.Add("2");
            _comboBoxHound.Items.Add("3");
            _comboBoxHound.Items.Add("4");

            //calling the method to create the bettors
            CreateBettors();
            //calling the method to create the race hounds
            CreateRaceHounds();

        }


        /// <summary>
        /// Create the race hound objects
        /// </summary>
        public void CreateRaceHounds()
        {
            //Stopping the timer
            _tmRaceTimer.Stop();

            //Inserting each greyhound object into a list 
            //Each race hound has an image, a start position, racetrack length, and a randomizer
            _raceHoundList.Insert(0, new Greyhound { img = _imgRacingHound1, StartPosition = Canvas.GetLeft(_imgRacingHound1), RaceTrackLength = _imgRaceTrack.Width - _imgRacingHound1.Width, Randomizer = _randomizer });
            _raceHoundList.Insert(1, new Greyhound { img = _imgRacingHound2, StartPosition = Canvas.GetLeft(_imgRacingHound2), RaceTrackLength = _imgRaceTrack.Width - _imgRacingHound2.Width, Randomizer = _randomizer });
            _raceHoundList.Insert(2, new Greyhound { img = _imgRacingHound3, StartPosition = Canvas.GetLeft(_imgRacingHound3), RaceTrackLength = _imgRaceTrack.Width - _imgRacingHound3.Width, Randomizer = _randomizer });
            _raceHoundList.Insert(3, new Greyhound { img = _imgRacingHound4, StartPosition = Canvas.GetLeft(_imgRacingHound4), RaceTrackLength = _imgRaceTrack.Width - _imgRacingHound4.Width, Randomizer = _randomizer });
        }

        /// <summary>
        /// Create the bettor objects
        /// </summary>
        private void CreateBettors()
        {
            //each bettor has a name, cash amount, and bet amount
            _crtSelBettor.Add(new Bettor() { _name = "Joe", _cash = 10, HasPlacedBet = new Bet()});
            _crtSelBettor.Add(new Bettor() { _name = "Bob", _cash = 100, HasPlacedBet = new Bet() });
            _crtSelBettor.Add(new Bettor() { _name = "Anna", _cash = 50, HasPlacedBet = new Bet() });
           
            
        }

       //this method is called when either three radio buttons are clicked
        private void OnBettorSelectorClicked(object sender, RoutedEventArgs e)
        {
            //changing the labels of the radio button when it is clicked
            //displays their name and current cash amount
           _radioBtnJoe.Content = "Joe has "+_crtSelBettor[0]._cash+ " dollars";
           _radioBtnBob.Content = "Bob has " + _crtSelBettor[1]._cash + " dollars";
           _radioBtnAnna.Content = "Anna has " + _crtSelBettor[2]._cash + " dollars";

            //Chenges who is betting labels when their radio button is clicked
            if (_radioBtnJoe.IsChecked == true)
            {
                _txtSelectedBettorLabel.Text = _crtSelBettor[0]._name + " bets";
            }
            else if (_radioBtnJoe.IsChecked == true)
            {
                _txtSelectedBettorLabel.Text = _crtSelBettor[1]._name + " bets";
            }
            else
            {
                _txtSelectedBettorLabel.Text = _crtSelBettor[2]._name + " bets";
            }
        }

        //checks when the 'place bet' button is clicked
        //I have 'async' with this method because its needed if you want to show a popupu message
        private async void OnPlaceBet(object sender, RoutedEventArgs e)
        {
            //checks if joes radio button is currentyl highlighted
            if (_radioBtnJoe.IsChecked == true)
            {
                //sets the current value in _txtBetAmount to an integer called bet1
                int bet1 = int.Parse(_txtbetAmount.Text);

                //Gets the value that is currently selected in the dropdown menu and sets that integer value to houndNum
                int houndNum = int.Parse(_comboBoxHound.SelectedItem as string);

                //boolean value to check whether a bet has been placed or not
                bool betPlaced;

                
                _crtSelBettor[0]._name = _txtSelectedBettorLabel.Text;

                //checks to see if the bet amount is larger than the minimmum bet which is 5
                if (bet1 >= minBet)
                {
                    //checks to make sure that placebet returns valid bet1 values and hound Num value
                    betPlaced = _crtSelBettor[0].PlaceBet(bet1, houndNum);

                    if (betPlaced)
                    {
                        //Update the labels
                        _crtSelBettor[0].UpdateLabels();
                        BetPlaced = true;
                    }
                }

                //Updates the description
                _txtBet1.Text = ("Joe bet $" + bet1 + " on hound #" + houndNum);
               
               
            }
            
            //The next 2 else if's do the same as the top if statement except it is for bob and Anna
            //I know this could have been done much better but couldn't get it working any other way
            else if (_radioBtnBob.IsChecked == true)
            {


                int bet2 = int.Parse(_txtbetAmount.Text);
                int houndNum = int.Parse(_comboBoxHound.SelectedItem as string);
                bool betPlaced;

                _crtSelBettor[1]._name = _txtSelectedBettorLabel.Text;

                if (bet2 >= minBet)
                {
                    betPlaced = _crtSelBettor[1].PlaceBet(bet2, houndNum);

                    if (betPlaced)
                    {
                        _crtSelBettor[1].UpdateLabels();
                        BetPlaced = true;
                    }
                }

                _txtBet2.Text = ("Bob bet $" + bet2 + " on hound #" + houndNum);
            }

            else if (_radioBtnAnna.IsChecked == true)
            {


                int bet3 = int.Parse(_txtbetAmount.Text);
                int houndNum = int.Parse(_comboBoxHound.SelectedItem as string);
                bool betPlaced;

                _crtSelBettor[2]._name = _txtSelectedBettorLabel.Text;

                if (bet3 >= minBet)
                {
                    betPlaced = _crtSelBettor[2].PlaceBet(bet3, houndNum);

                    if (betPlaced)
                    {
                        _crtSelBettor[2].UpdateLabels();
                        BetPlaced = true;
                    }
                }

                _txtBet3.Text = ("Anna bet $" + bet3 + " on hound #" + houndNum);
            }

            //If the program gets to this point, no radio button has been clicked when place bet button was clicked
            else
            {
                var noBets = new MessageDialog("NO BETS HAVE BEEN PLACED!");
                await noBets.ShowAsync();
            }


        }

        /// <summary>
        /// Called when the race button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnStartRace(object sender, RoutedEventArgs e)
        {
            //checks if any bets have been placed
            //If not, the race will not begin
            if(BetPlaced == false)
            {
                var noBets = new MessageDialog("NO BETS HAVE BEEN PLACED!");
                await noBets.ShowAsync();
            }
            //Bets have been placed and race will start
            else
            {
                //disable the place bet button and start race button
                _btnPlaceBet.IsEnabled = false;
                _btnStartRace.IsEnabled = false;

                //start the timer
                _tmRaceTimer.Start();
            }
            
           

        }

        /// <summary>
        /// Start the timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnRaceTimerTick(object sender, object e)
        {
            //Goes through each hound in the race houndist and makes them run, as well as checks if they win
            foreach (Greyhound hound in _raceHoundList)
            {
                //calls the running method for the hounds and checks whther it is true
                //If it is true, that means a hound has won the race.
                //If it returns false, that means no hound has won the race
                if (hound.Run())
                {
                    //Stop the timer as a hound has won the race
                    _tmRaceTimer.Stop();
                    
                    //create a new variable integer to store the nummber of the winning hound
                    int winningHound = _raceHoundList.IndexOf(hound) + 1;

                    //show the winning message popup on the screen as well as which hound won
                    var winMessage = new MessageDialog("Hound #"+winningHound+" won!");
                    await winMessage.ShowAsync();

                    //Go ahead and enable both of the buttons back
                    _btnPlaceBet.IsEnabled = true;
                    _btnStartRace.IsEnabled = true;

                    //go through each bettor in the betting list and call the collect method
                    //to check if they won
                    foreach (Bettor bettor in _crtSelBettor)
                    {
                        bettor.Collect(winningHound);

                    }

                    //reset the hounds back to their starting positions
                    foreach (Greyhound _hound in _raceHoundList)
                    {
                        _hound.TakeStartingPosition();
                    }

                }

            }
        }
    }
}
