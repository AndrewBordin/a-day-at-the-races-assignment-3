﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace RaceTrackSim
{
    class Greyhound
    {
        //This is the location that is going to be added on top of the location for the
        //hounds so that they will move
        private int _locationToAdd;

        //The starting position of the hounds
        public double _startPosition;

        //The current location of the hounds
        public int _location;

        //The length of the race track
        public double _raceTrackLength;

        public double StartPosition
        {
            get { return _startPosition; }
            set { _startPosition = value; }
        }

        private int Location
        {
            get { return _location; }
            set { _location = value; }
        }
        public double RaceTrackLength
        {
            get { return _raceTrackLength; }
            set { _raceTrackLength = value; }
        }

        public Random Randomizer;

        //represents the images of the hounds
        public Image img;
  
        /// <summary>
        /// Constructor initializes the greyhound location to zero
        /// </summary>
        public Greyhound()
        {
            _location = 0;
        }

        /// <summary>
        /// Moves each hound to their respective starting positions
        /// </summary>
        public void TakeStartingPosition()
        {
           
            _location = 0;
            _locationToAdd = 0;
            Canvas.SetLeft(img, StartPosition);
        }

        /// <summary>
        /// Move the greyhounds
        /// </summary>
        /// <returns></returns>
        public bool Run()
        {
            //randomizer for the location of the greyhounds
            _location = Randomizer.Next(1, 10);

            //Add the number from randomizer to location to add
            _locationToAdd += _location;

            //Move the image
            Canvas.SetLeft(img, StartPosition + _locationToAdd);

            //Checks if someone has won the race
            if (Canvas.GetLeft(img) >= RaceTrackLength)
            {
                return true;
            }
            else
            {
                return false;
            }   
        }
    }
}
