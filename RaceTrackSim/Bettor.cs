﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace RaceTrackSim
{
    class Bettor 
    {
        //Bet object which is different for each bettor
        public Bet HasPlacedBet;

        //The name of each bettor
        public string _name;

        //The cash each bettor has
        public int _cash;

        private int Cash
        {
            get { return _cash; }
            set { _cash = value; }
        }

        private string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        

        public Bettor()
        {
            _name = Name;
            _cash = Cash;
        }

        /// <summary>
        /// Gets called when a bettor has placed a bet
        /// creates new bet object that has a bet amount, the hound in which they chose
        /// </summary>
        /// <param name="betAmount"></param>
        /// <param name="houndToWin"></param>
        /// <returns></returns>
        public bool PlaceBet(int betAmount, int houndToWin)
        {
            HasPlacedBet = new Bet { Amount = betAmount, RaceHound = houndToWin, _bettor = this};
            return (_cash >= betAmount);
        }

        //TODO: clear bets after the race is finished
        public void ClearBet()
        {
            
        }

        /// <summary>
        /// Called when a bettor has won and their cash amount needs to be updated
        /// </summary>
        /// <param name="winnerHound"></param>
        public void Collect(int winnerHound)
        {
             if (HasPlacedBet != null)
            {
                Cash += HasPlacedBet.PayOut(winnerHound);
            }
        }

        //TODO: Get this working so that the labels update immediately after a bet has been placed
        public  void UpdateLabels()
        {
            
        }




    }
}
